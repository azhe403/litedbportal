using System;
using System.Collections.ObjectModel;
using LiteDBPortal.ViewModels;
using ReactiveUI;

namespace LiteDBPortal
{
    public class QueryManagementService : ReactiveObject
    {
        private static readonly Lazy<QueryManagementService> SingletonLazy =
            new Lazy<QueryManagementService>(() => new QueryManagementService());

        private QueryViewModel _currentQuery;
        
        public static QueryManagementService Instance => SingletonLazy.Value;
        
        public ObservableCollection<DatabaseViewModel> Databases { get; }

        public ObservableCollection<QueryViewModel> QueryViewModels { get; }

        public QueryViewModel CurrentQuery
        {
            get => _currentQuery;
            set => this.RaiseAndSetIfChanged(ref _currentQuery, value);
        }
        
        public void AddNewQueryViewModel()
        {
            var newQueryViewModel = new QueryViewModel(Databases)
            {
                Title = $"Query {++QueryCreatedCounter}"
            };

            if (QueryViewModels.Count == 0)
            {
                CurrentQuery = newQueryViewModel;
            }
            
            QueryViewModels.Add(newQueryViewModel);
        }


        public void RemoveQuery(QueryViewModel requestRemove)
        {
            QueryViewModels.Remove(requestRemove);
            if (QueryViewModels.Count == 0)
            {
                AddNewQueryViewModel();
            }
        }
        
        private int QueryCreatedCounter { get; set; }

        private QueryManagementService()
        {
            Databases = new ObservableCollection<DatabaseViewModel>();
            QueryViewModels = new ObservableCollection<QueryViewModel>();
        }

    }
}
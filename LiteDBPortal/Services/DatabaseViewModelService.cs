using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using LiteDB;
using LiteDBPortal.Services;
using LiteDBPortal.ViewModels;
using LiteDBPortal.Views;
using MessageBox.Avalonia;
using MessageBox.Avalonia.Enums;

namespace LiteDBPortal.Generator
{
    public class DatabaseViewModelService
    {
        public static bool CloseDatabase(DatabaseViewModel dbToClose)
        {
            var currentApplication = Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            var mainViewModel = currentApplication?.MainWindow.DataContext as MainWindowViewModel;

            if (mainViewModel == null)
            {
                return false;
            }
            
            mainViewModel.CloseDatabase(dbToClose);

            return true;
        }
        
        public static async Task<(bool success, DatabaseViewModel vm)> ConnectToDb(string filepath)
        {
            var emptyVm = new DatabaseViewModel();
            if (string.IsNullOrWhiteSpace(filepath))
            {
                await DialogBoxService.ShowErrorDialog("DB File Error",
                    $"Path to database file should not be null or empty space: {filepath}");

                return (false, emptyVm);
            }

            var fileInfo = new FileInfo(filepath);

            if (!fileInfo.Exists)
            {
                await DialogBoxService.ShowErrorDialog("DB File Error",
                    $"Requested path does not exist: {filepath}");
                
                return (false, emptyVm);
            }

            if (fileInfo.IsReadOnly)
            {
                await DialogBoxService.ShowErrorDialog("DB File Error",
                    $"Requested path can only be read but needs read/write access: {filepath}");
                
                return (false, emptyVm);
            }

            try
            {
                var db = new LiteDatabase(filepath);
                return (true, new DatabaseViewModel(fileInfo, db));
            }
            catch (Exception e)
            {
                await DialogBoxService.ShowErrorDialog("DB File Error",
                    $"Exception through accessing {filepath}: {e.Message}");
                
                return (false, emptyVm);
            }

        }

        public static async Task<(bool success, List<string> generatedFiles)> ExportDatabase(DatabaseViewModel dbViewModel, string baseFolder)
        {
            var baseFolderInfo = new DirectoryInfo(baseFolder);

            if (!baseFolderInfo.Exists)
            {
                await DialogBoxService.ShowErrorDialog("File Error",
                    $"Requested export directory doesn't exist: {baseFolderInfo.FullName}");
                return (false, new List<string>());
            }

            var collections = dbViewModel.Database.GetCollectionNames().ToList();
            var createdFiles = new List<string>();
            foreach (var collection in collections)
            {
                var fileName = $"{collection}.json";
                var filePath = Path.Combine(baseFolder, fileName);
                var exportQuery = $"SELECT $ INTO $file_json('{filePath}') FROM {collection};";
                try
                {
                    var result = dbViewModel.Database.Execute(exportQuery);
                    createdFiles.Add(filePath);
                }
                catch
                {
                    var response = await DialogBoxService.ShowOkCancelDialogBox("File Error",
                        $"Error exporting {collection} to : {filePath}" 
                        + Environment.NewLine +
                        "Continue on with export?");
                    if (response == ButtonResult.Cancel)
                    {
                        break;
                    }

                    return (false, createdFiles);
                }
            }

            return (true, createdFiles);
        }
        
        public static async Task<(bool success, DatabaseViewModel vm)> GenerateNewDatabase(string filepath)
        {
            var emptyVm = new DatabaseViewModel();
            if (string.IsNullOrWhiteSpace(filepath))
            {
                await DialogBoxService.ShowErrorDialog("DB File Error",
                    $"Path to database file should not be null or empty space: {filepath}");

                return (false, emptyVm);
            }

            var fileInfo = new FileInfo(filepath);

            try
            {
                var db = new LiteDatabase(filepath);
                return (true, new DatabaseViewModel(fileInfo, db));
            }
            catch (Exception e)
            {
                await DialogBoxService.ShowErrorDialog("DB File Error",
                    $"Exception through accessing {filepath}: {e.Message}");
                
                return (false, emptyVm);
            }

        }
        
    }
}
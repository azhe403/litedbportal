using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using MessageBox.Avalonia;
using MessageBox.Avalonia.Enums;

namespace LiteDBPortal.Services
{
    public class DialogBoxService
    {
        public static async Task ShowErrorDialog(string title, string message)
        {
            var application = Application.Current?.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            if (application == null)
            {
                return;
            }

            await MessageBoxManager.GetMessageBoxStandardWindow(title, message).ShowDialog(application.MainWindow);
        }
        
        public static async Task<ButtonResult> ShowOkCancelDialogBox(string title, string message)
        {
            var application = Application.Current?.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            if (application == null)
            {
                return ButtonResult.Ok;
            }

            return await MessageBoxManager.GetMessageBoxStandardWindow(title, message, ButtonEnum.OkCancel)
                .ShowDialog(application.MainWindow);
        }

    }
}
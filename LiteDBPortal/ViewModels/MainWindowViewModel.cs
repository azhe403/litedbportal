﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reactive;
using System.Threading.Tasks;
using System.Timers;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using LiteDB;
using LiteDBPortal.Generator;
using MessageBox.Avalonia;
using ReactiveUI;
using static LiteDBPortal.Services.DialogBoxService;

namespace LiteDBPortal.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public const string DefaultTitle = "LiteDB Portal Release 1";
        public const string ProjectUrl = "https://gitlab.com/HankG/litedbportal";

        private readonly string _defaultInformation = DefaultTitle 
                                                      + Environment.NewLine 
                                                      + "Copyright 2019 Hank Grabowski" 
                                                      + Environment.NewLine 
                                                      + ProjectUrl;
        private string _title;
        private const double StatusUpdateDurationMillisecond = 10_000;
        private string _status;
        private Timer _statusTimer;
        private QueryViewModel _selectedQueryViewModel;

        public DatabaseBrowserViewModel DatabaseBrowserViewModel { get; }

        public ObservableCollection<QueryViewModel> QueryViewModels => QueryManagementService.Instance.QueryViewModels;

        public QueryViewModel SelectedQueryViewModel
        {
            get => _selectedQueryViewModel;
            set
            {
                QueryManagementService.Instance.CurrentQuery = value;
                this.RaiseAndSetIfChanged(ref _selectedQueryViewModel, value);
            }
        }

        public ReactiveCommand<Unit, Unit> AddNewQueryCmd { get; }
        
        public ReactiveCommand<Unit, Unit> OpenDatabaseCmd { get; }
        
        public ReactiveCommand<Unit, Task> GetInformationCmd { get; }

        public ReactiveCommand<Unit, Unit> NewDatabaseCmd { get; }

        public ReactiveCommand<Unit, Unit> TemplateGenArrayCmd { get; }

        public ReactiveCommand<Unit, Unit> TemplateDocArrayCmd { get; }

        public ReactiveCommand<Unit, Unit> TemplateGenComplexDocCmd { get; }

        public ReactiveCommand<Unit, Unit> TemplateDateTimeCmd { get; }

        public ReactiveCommand<Unit, Unit> TemplateGenIdCmd { get; }

        public ReactiveCommand<Unit, Unit> TemplateGenNumericCmd { get; }

        public ReactiveCommand<Unit, Unit> TemplateGenStringCmd { get; }

        public ReactiveCommand<Unit, Unit> TemplateQueryAggregationCmd { get; }

        public ReactiveCommand<Unit, Unit> TemplateQueryDistinctCmd { get; }

        public ReactiveCommand<Unit, Unit> TemplateQueryDistinctSubFieldCmd { get; }
        
        public ReactiveCommand<Unit, Unit> TemplateInsertComplexQuery { get; }
        
        public ReactiveCommand<Unit, Unit> TemplateInsertManyComplexQuery { get; }
        
        public ReactiveCommand<Unit, Unit> TemplateUpdateOneField { get; }
        
        public ReactiveCommand<Unit, Unit> TemplateUpdateWholeDocument { get; }

        public ReactiveCommand<Unit, Unit> TemplateUpdateManyDocuments { get; }
        
        public ReactiveCommand<Unit, Unit> TemplateDateTimeQueryCmd { get; }
        
        public ReactiveCommand<Unit, Unit> TemplateStringContainsCmd { get; }
        
        public ReactiveCommand<Unit, Unit> TemplateDeleteOneCmd { get; }
        
        public ReactiveCommand<Unit, Unit> TemplateDeleteManyCmd { get; }
        
        public ReactiveCommand<Unit, Unit> TemplateDeleteAllCmd { get; }

        public string Title
        {
            get => _title;
            set => this.RaiseAndSetIfChanged(ref _title, value);
        }

        public string Status
        {
            get => _status;
            set => this.RaiseAndSetIfChanged(ref _status, value);
        }

        public MainWindowViewModel()
        {
            Title = DefaultTitle;
            DatabaseBrowserViewModel = new DatabaseBrowserViewModel();
            AddNewQueryViewModel();
            OpenDatabasePaths = new HashSet<string>();
            OpenDatabaseCmd = ReactiveCommand.Create(OpenDatabase);
            NewDatabaseCmd = ReactiveCommand.Create(NewDatabase);
            AddNewQueryCmd = ReactiveCommand.Create(AddNewQueryViewModel);
            GetInformationCmd =
                ReactiveCommand.Create(async () =>
                {
                    await ShowErrorDialog(DefaultTitle, _defaultInformation);
                });
            TemplateGenArrayCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("Array", QueryTemplateGenerator.BuildNewArrayFieldTemplate()));
            TemplateDocArrayCmd =  ReactiveCommand.Create(() => CopyTemplateToClipboard("Array of Complex Document", QueryTemplateGenerator.BuildNewComplexArrayOfDocuments()));
            TemplateGenComplexDocCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("Complex Document", QueryTemplateGenerator.BuildNewComplexDocument()));
            TemplateDateTimeCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("DateTime", QueryTemplateGenerator.BuildNewDateTimeTemplate()));
            TemplateGenIdCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("ID", QueryTemplateGenerator.BuildNewGuidTemplate()));
            TemplateGenNumericCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("Numeric", QueryTemplateGenerator.BuildNewNumericFieldTemplate()));
            TemplateGenStringCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("String", QueryTemplateGenerator.BuildNewStringFieldTemplate()));
            TemplateQueryAggregationCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("Aggregation Query", QueryTemplateGenerator.BuildAggregationQuery()));
            TemplateQueryDistinctCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("Distinct Query", QueryTemplateGenerator.BuildDistinctNameQuery()));
            TemplateQueryDistinctSubFieldCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("Distinct Query on Sub-field", QueryTemplateGenerator.BuildDistinctNameSubFieldQuery()));
            TemplateInsertComplexQuery = ReactiveCommand.Create(() => CopyTemplateToClipboard("Insert Document", QueryTemplateGenerator.BuildInsertSingleQuery()));
            TemplateInsertManyComplexQuery = ReactiveCommand.Create(() => CopyTemplateToClipboard("Insert Many Document", QueryTemplateGenerator.BuildInsertManyQuery()));
            TemplateUpdateOneField =  ReactiveCommand.Create(() => CopyTemplateToClipboard("Update Field of a Document", QueryTemplateGenerator.BuildUpdateOneField()));
            TemplateUpdateWholeDocument =  ReactiveCommand.Create(() => CopyTemplateToClipboard("Update Whole document", QueryTemplateGenerator.BuildUpdateDocument()));
            TemplateUpdateManyDocuments =  ReactiveCommand.Create(() => CopyTemplateToClipboard("Update Many documents", QueryTemplateGenerator.BuildUpdateManyDocuments()));
            TemplateDateTimeQueryCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("Query Between Times", QueryTemplateGenerator.BuildQueryBetweenDates()));
            TemplateStringContainsCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("Query string contains(like)", QueryTemplateGenerator.BuildQueryStringContains()));
            TemplateDeleteOneCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("Delete one object", QueryTemplateGenerator.BuildDeleteOne()));
            TemplateDeleteManyCmd =  ReactiveCommand.Create(() => CopyTemplateToClipboard("Delete many objects", QueryTemplateGenerator.BuildDeleteMany()));
            TemplateDeleteAllCmd = ReactiveCommand.Create(() => CopyTemplateToClipboard("Delete all objects", QueryTemplateGenerator.BuildDeleteAll()));
            
            _statusTimer = new Timer(StatusUpdateDurationMillisecond);
            _statusTimer.Elapsed += (sender, args) => ResetStatus();
            _statusTimer.AutoReset = true;
            _statusTimer.Enabled = true;
            SetNewStatus("Initial Status good");
        }

        public void AddNewQueryViewModel()
        {
            QueryManagementService.Instance.AddNewQueryViewModel();
        }

        private void CopyTemplateToClipboard(string type, string value)
        {
            Application.Current.Clipboard.SetTextAsync(value);
            SetNewStatus($"{type} template copied to clipboard");
        }

        public async void CloseDatabase(DatabaseViewModel databaseViewModel)
        {
            if (databaseViewModel == null)
            {
                await ShowErrorDialog("LiteDB Portal Error",
                    "Please Select a Database to close");
                return;
            }
            
            databaseViewModel.Database.Dispose();
            OpenDatabasePaths.Remove(databaseViewModel.Filepath);
            QueryManagementService.Instance.Databases.Remove(databaseViewModel);
        }

        public async void NewDatabase()
        {
            var databaseFile = await GetNewDatabaseFileName();
            if (string.IsNullOrWhiteSpace(databaseFile))
            {
                return;
            }

            var (success, vm) = await DatabaseViewModelService.GenerateNewDatabase(databaseFile);
            if (!success)
            {
                await ShowErrorDialog("LiteDB Error",
                    $"Failed creating database at: {databaseFile}");
                return;
            }

            QueryManagementService.Instance.Databases.Add(vm);
            OpenDatabasePaths.Add(vm.Filepath);

        }

        public async void OpenDatabase()
        {
            var databaseFile = await GetDatabaseFileName();
            if (string.IsNullOrWhiteSpace(databaseFile))
            {
                return;
            }
            
            var dbFileInfo = new FileInfo(databaseFile);
            if (OpenDatabasePaths.Contains(dbFileInfo.FullName))
            {
                await ShowErrorDialog("LiteDB Error",
                    $"The LiteDB database file at the requested location is already loaded: {dbFileInfo.FullName}");
                return;
            }
            var (success, vm) = await DatabaseViewModelService.ConnectToDb(databaseFile);
            if (!success)
            {
                await ShowErrorDialog("LiteDB Error",
                    $"Failed loading database at: {dbFileInfo.FullName}");
                return;
            }

            QueryManagementService.Instance.Databases.Add(vm);
            OpenDatabasePaths.Add(vm.Filepath);
        }

        public void SetNewStatus(string newString)
        {
            Status = "Status: " + newString;
        }

        private void ResetStatus()
        {
            SetNewStatus("OK");
        }
        
        private async Task<string> GetDatabaseFileName()
        {
            var app = Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            var window = app.MainWindow;
            var fileDialog = new OpenFileDialog
            {
                Directory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                Title = "Open Database",
                AllowMultiple = false,
            };
            
            var selection = await fileDialog.ShowAsync(window).ConfigureAwait(false);

            if (selection != null && selection.Length > 0)
            {
                return selection[0];
            }

            return "";
        }

        private async Task<string> GetNewDatabaseFileName()
        {
            var app = Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            var window = app.MainWindow;
            var fileDialog = new SaveFileDialog()
            {
                
                Directory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                Title = "New Database",
                DefaultExtension = "db",
                InitialFileName = "NewLiteDatabase"
            };
            
            var selection = await fileDialog.ShowAsync(window).ConfigureAwait(false);
            return selection;

        }
        
        private HashSet<string> OpenDatabasePaths { get; }

    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using Avalonia;
using Avalonia.Media.Imaging;
using LiteDB;
using LiteDBPortal.Generator;

namespace LiteDBPortal.ViewModels
{
    public class QueryResultsTreeNodeModel : TreeNodeViewModel
    {
        public QueryResultsTreeNodeModel(string text, Bitmap icon, object nativeData = default) : base(text, icon, nativeData)
        {
        }

        public void CopyUpdateQueryToClipboard()
        {
            var bsonDocument = this.NativeData as BsonDocument;
            if (bsonDocument == null)
            {
                Console.WriteLine("native data wasn't bson");
                return;
            }

            var collectionName = QueryManagementService.Instance.CurrentQuery.CurrentCollection;
            var bsonData = QueryResultGenerator.GetJson(bsonDocument);
            var lines = bsonData.Split(Environment.NewLine);
            var filteredOutId = lines.ToList().FindAll(l => !l.Trim().StartsWith("\"_id\":"));
            var finalJson = string.Join(Environment.NewLine, filteredOutId);
            var idStringRaw = bsonDocument["_id"];
            var idString = "";
            if (bsonDocument["_id"].Type == BsonType.ObjectId)
            {
                idString = $"ObjectId(\"{idStringRaw.AsObjectId.ToString()}\")";
            }
            else
            {
                idString = idStringRaw.ToString();
            }
            var query =  $"UPDATE {collectionName}" + Environment.NewLine +
                         "SET" + Environment.NewLine +
                         finalJson +
                         $"WHERE _id={idString}";
            Application.Current.Clipboard.SetTextAsync(query);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
using Avalonia.Controls;
using DynamicData;
using LiteDB;
using LiteDBPortal.Generator;
using LiteDBPortal.Services;
using LiteDBPortal.Views;
using MessageBox.Avalonia;
using ReactiveUI;

namespace LiteDBPortal.ViewModels
{
    public class QueryViewModel : ViewModelBase
    {
        private const int DefaultMaxSettingsintIndex = 2;
        private string _currentCollection;
        private int _selectedMaxCount;
        private int _currentMaxCount;
        private int _currentPageMax;
        private int _currentPageMin;
        private string _currentQueryStatus;
        private string _queryText;
        private string _queryResults;
        private string _title;
        private DatabaseViewModel _selectedDatabaseViewModel;
        
        public List<int> MaxSettings { get; } = new List<int>{5, 10, 20, 50, 100, 200, 500, 1000};

        public string CurrentCollection
        {
            get => _currentCollection;
            set => this.RaiseAndSetIfChanged(ref _currentCollection, value);
        }

        public int SelectedMaxCount
        {
            get => _selectedMaxCount;
            set
            {
                CurrentMaxCount = MaxSettings[value];
                lastQueryCount = NoPreviousQueryValue;
                this.RaiseAndSetIfChanged(ref _selectedMaxCount, value);
            }
        }

        public string Title
        {
            get => _title;
            set => this.RaiseAndSetIfChanged(ref _title, value);
        }

        public int CurrentMaxCount
        {
            get => _currentMaxCount;
            set => this.RaiseAndSetIfChanged(ref _currentMaxCount, value);
        }

        public int CurrentPageMax
        {
            get => _currentPageMax;
            set => this.RaiseAndSetIfChanged(ref _currentPageMax, value);
        }

        public int CurrentPageMin
        {
            get => _currentPageMin;
            set => this.RaiseAndSetIfChanged(ref _currentPageMin, value);
        }

        public string CurrentQueryStatus
        {
            get => _currentQueryStatus;
            set => this.RaiseAndSetIfChanged(ref _currentQueryStatus, value);
        }

        public ObservableCollection<DatabaseViewModel> Databases { get; }
        
        public ReactiveCommand<Unit,Unit> CloseQueryCmd { get; }
        
        public ReactiveCommand<Unit,Unit> RunQueryCmd { get; }
        
        public ReactiveCommand<Unit,Unit> NextPageQueryCmd { get; }
        
        public ReactiveCommand<Unit,Unit> PreviousPageQueryCmd { get; }

        public DatabaseViewModel SelectedDatabaseViewModel
        {
            get => _selectedDatabaseViewModel;
            set
            {
                lastQueryCount = NoPreviousQueryValue;
                this.RaiseAndSetIfChanged(ref _selectedDatabaseViewModel, value);
                UpdateDatabaseSelection();
            }
        }

        public string QueryResults
        {
            get => _queryResults;
            set => this.RaiseAndSetIfChanged(ref _queryResults, value);
        }
        
        public ObservableCollection<QueryResultsTreeNodeModel> QueryResultsTreeView { get; }


        public string QueryText
        {
            get => _queryText;
            set
            {
                lastQueryCount = NoPreviousQueryValue;
                this.RaiseAndSetIfChanged(ref _queryText, value);
            }
        }

        public QueryViewModel(ObservableCollection<DatabaseViewModel> databases)
        {
            Databases = databases;
            CloseQueryCmd = ReactiveCommand.Create(() =>
            {
                QueryManagementService.Instance.RemoveQuery(this);
            });
            
            RunQueryCmd = ReactiveCommand.Create(() =>
            {
                CurrentPageMin = 0;
                CurrentPageMax = CurrentMaxCount;
                RunQuery();
            });
            NextPageQueryCmd = ReactiveCommand.Create(RunNextPageQuery);
            PreviousPageQueryCmd = ReactiveCommand.Create(RunPreviousPageQuery);
            QueryResultsTreeView = new ObservableCollection<QueryResultsTreeNodeModel>();
            SetDefaults();
        }

        public void SetDefaults()
        {
            CurrentCollection = "N/A";
            CurrentQueryStatus = "N/A";
            SelectedMaxCount = DefaultMaxSettingsintIndex;
            CurrentPageMin = 0;
            CurrentPageMax = 0;
            if (Databases.Count > 0)
            {
                SelectedDatabaseViewModel = Databases.First();
            }
        }
        
        public void UpdateDatabaseSelection()
        {
            CurrentDatabase = SelectedDatabaseViewModel?.Database;
        }

        private const int NoPreviousQueryValue = -1;
        private LiteDatabase CurrentDatabase { get; set; }

        private int lastQueryCount = NoPreviousQueryValue;

        private void RunPreviousPageQuery()
        {
            if (CurrentPageMin == 0)
            {
                RunQuery().Wait();
            }
            
            var max = CurrentPageMin;
            var min = max - CurrentMaxCount;
            if (min < 0)
            {
                min = 0;
            }

            CurrentPageMin = min;
            CurrentPageMax = max;
            RunQuery().Wait();
        }

        private async void RunNextPageQuery()
        {
            if (lastQueryCount > 0 && lastQueryCount < CurrentMaxCount)
            {
                await DialogBoxService.ShowErrorDialog("Query Warning", $"Last query was end of pages, can't page forward");
                return;
            }

            var originalMin = CurrentPageMin;
            var originalMax = CurrentPageMax;
            
            var min = CurrentPageMax;
            var max = min + CurrentMaxCount;

            CurrentPageMin = min;
            CurrentPageMax = max;
            if (!await RunQuery())
            {

                CurrentPageMin = originalMin;
                CurrentPageMax = originalMax;
            }
            
        }

        private async Task<bool> RunQuery()
        {
            if (string.IsNullOrWhiteSpace(QueryText))
            {
                SetMessageToOutput("Invalid query, cannot be null or empty", true);
                return false;
            }

            if (CurrentDatabase == null)
            {
                SetMessageToOutput("Please select a database to use for query", true);
                return false;
            }

            if (CurrentPageMin == CurrentPageMax)
            {
                CurrentPageMax = CurrentPageMin + CurrentMaxCount;
            }
            
            if (CurrentPageMin > CurrentPageMax)
            {
                var actualMin = _currentPageMax;
                CurrentPageMax = CurrentPageMin;
                CurrentPageMin = actualMin;
            }

            try
            {
                var query = QueryText.Trim();
                CurrentCollection = QueryParsingService.ExtractCollectionName(query);
                var pagedQuery =
                    QueryParsingService.GenerateQueryWithMinMax(query, CurrentPageMin, CurrentPageMax);
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                var results = CurrentDatabase.Execute(pagedQuery);
                if (!results.HasValues)
                {
                    SetMessageToOutput("No results", false);
                    return false;
                }

                var resultsEnumerable = results.ToList();
                lastQueryCount = resultsEnumerable.Count();
                CurrentPageMax = CurrentPageMin + resultsEnumerable.Count();
                QueryResults = QueryResultGenerator.GetJson(resultsEnumerable, CurrentPageMin);
                var treeViewResults = QueryResultGenerator.GetTreeViewNode(resultsEnumerable, CurrentPageMin);
                QueryResultsTreeView.Clear();
                QueryResultsTreeView.AddRange(treeViewResults);
                stopWatch.Stop();
                SetStatusFromStopwatch(stopWatch);
                if (QueryParsingService.QueryMayChangeDatabase(query))
                {
                    SelectedDatabaseViewModel.RefreshProperties();
                }
            }
            catch (Exception e)
            {
                SetMessageToOutput(e.ToString(), true);
                return false;
            }

            return true;
        }

        private void SetStatusFromStopwatch(Stopwatch stopwatch)
        {
            var elapsedTime = stopwatch.Elapsed;
            var totalSeconds = elapsedTime.TotalSeconds;
            if (totalSeconds < 1.0)
            {
                CurrentQueryStatus = $"{elapsedTime.TotalMilliseconds:0.000} ms";
                return;
            }

            if (totalSeconds < 600.0)
            {
                CurrentQueryStatus = $"{elapsedTime.TotalSeconds:0.000} sec";
            }

            CurrentQueryStatus = $"{elapsedTime.TotalMinutes:0.000} min";

        }

        private void SetMessageToOutput(string message, bool isError)
        {
            var icon = isError ? IconGenerator.ErrorIcon : IconGenerator.InfoIcon;
            QueryResultsTreeView.Clear();
            QueryResultsTreeView.Add(new QueryResultsTreeNodeModel(message, icon, message));
            QueryResults = message;
        }
    }
}
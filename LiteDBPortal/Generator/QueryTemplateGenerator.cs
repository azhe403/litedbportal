using System;
using System.Globalization;
using System.Text;

namespace LiteDBPortal.Generator
{
    public static class QueryTemplateGenerator
    {
        public static string BuildNewNumericFieldTemplate()
        {
            return "\"new_field_name\" : 10";
        }
        
        public static string BuildNewStringFieldTemplate()
        {
            return "\"new_field_name\" : \"new string value\"";
        }

        public static string BuildNewDateTimeTemplate()
        {
            return "\"new_field_name\" : {\"$date\": \"2019-11-25T19:43:50.4240000Z\"}";
        }
        
        public static string BuildNewGuidTemplate()
        {
            return $"\"_id\" : \"{Guid.NewGuid().ToString()}\"";
        }

        public static string BuildNewArrayFieldTemplate()
        {
            return "\"new_field_name\" : [ <value1>, <value2>, <value3>]";
        }

        public static string BuildNewComplexDocument()
        {
            return "{" + Environment.NewLine +
                   "  \"new_field1\" : \"string value 1\","  + Environment.NewLine +
                   "  \"new_field2\" : 10,"  + Environment.NewLine +
                   "  \"new_field3\" : [ 10, 20, 30],"  + Environment.NewLine +
                   "  \"new_field4\" : {"  + Environment.NewLine +
                   "    \"sub_field1\" : \"string value sub field 1\"," + Environment.NewLine  +
                   "    \"sub_field2\" : 100" + Environment.NewLine +
                   "  }" + Environment.NewLine +
                   "}";
        }

        public static string BuildNewComplexArrayOfDocuments()
        {
            return "[" + Environment.NewLine +
                   BuildNewComplexDocument() + "," + Environment.NewLine +
                   BuildNewComplexDocument() + "," + Environment.NewLine +
                   BuildNewComplexDocument() + Environment.NewLine +
                   "]";
        }

        public static string BuildAggregationQuery()
        {
            return "SELECT age," + Environment.NewLine +
                   "  COUNT(*) AS total," + Environment.NewLine +
                   "  [MIN(*.name), MAX(*.name)] names" + Environment.NewLine +
                   "FROM <Collection Name>" + Environment.NewLine +
                   "WHERE age > 25" + Environment.NewLine +
                   "GROUP BY age";
        }
        
        public static string BuildDistinctNameQuery()
        {
            return "SELECT DISTINCT(*.name)" + Environment.NewLine +
                   "FROM <Collection Name>";
        }

        public static string BuildDistinctNameSubFieldQuery()
        {
            return "SELECT DISTINCT(*.person.name)" + Environment.NewLine +
                   "FROM <Collection Name>";
        }

        public static string BuildInsertSingleQuery()
        {
            return "INSERT INTO <Collection_Name> VALUES" + Environment.NewLine +
                   BuildNewComplexDocument();
        }
        
        public static string BuildInsertManyQuery()
        {
            return "INSERT INTO <Collection_Name> VALUES" + Environment.NewLine +
                   BuildNewComplexDocument() + "," + Environment.NewLine +
                   BuildNewComplexDocument();
        }

        public static string BuildUpdateOneField()
        {
            return "UPDATE <Collection Name>" + Environment.NewLine +
                   "SET age=60" + Environment.NewLine +
                   "WHERE _id=ObjectId(\"5de40aac4875ee0f909e80ac\")";
        }
        
        public static string BuildUpdateDocument()
        {
            return "UPDATE <Collection Name>" + Environment.NewLine +
                   "SET" + Environment.NewLine +
                   "{" + Environment.NewLine +
                   "  \"name\": \"New Name\"," + Environment.NewLine +
                   "  \"age\": 41" + Environment.NewLine +
                   "}" + Environment.NewLine +
                   "WHERE _id=ObjectId(\"5de40aac4875ee0f909e80ac\")";
        }

        public static string BuildUpdateManyDocuments()
        {
            return "UPDATE <Collection Name>" + Environment.NewLine +
                   "SET valid=true" + Environment.NewLine +
                   "WHERE age > 35";
        }

        public static string BuildQueryBetweenDates()
        {
            return "SELECT $ FROM <Collection Name>" + Environment.NewLine +
                   "WHERE CreationTime BETWEEN DateTime(\"2019-11-28T00:00:00.00Z\") AND DateTime(\"2019-11-28T02:00:00.00+00:00Z\")";
        }

        public static string BuildQueryStringContains()
        {
            return "SELECT $ FROM <Collection Name>" + Environment.NewLine +
                   "WHERE TextField LIKE \"%supercomp%\"";
        }

        public static string BuildDeleteOne()
        {
            return "DELETE <Collection Name>" + Environment.NewLine +
                   "WHERE _id=ObjectId(\"5de40aac4875ee0f909e80ac\")";
        }
        
        public static string BuildDeleteMany()
        {
            return "DELETE <Collection Name>" + Environment.NewLine +
                   "WHERE valid != true";
        }

        public static string BuildDeleteAll()
        {
            return "DELETE <Collection Name> WHERE true=true";
        }

    }
}
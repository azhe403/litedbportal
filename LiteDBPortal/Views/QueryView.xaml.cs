using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using LiteDBPortal.ViewModels;

namespace LiteDBPortal.Views
{
    public class QueryView : UserControl
    {
        public QueryView()
        {
            InitializeComponent();
            InitializeCallbacks();
        }

        private void InitializeCallbacks()
        {

        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace LiteDBPortal.Views
{
    public class DatabaseBrowserView : UserControl
    {
        public DatabaseBrowserView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}